# GroovyServicePattern

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

This is a service/repository set of bases including bases for controller, service, repository and model.

The model encapsulates the filterable, sortable, and validation data and includes hooks for customization on a per model basis.

The respository encapsulates all database operations including transactions, where, sort, and eager loads.  It includes pre and post actions (eg. tranforms, validations) and a shared messaging component to return verbose errors and meta data (eg. query debug log entries).

The service encapsulates some pre and post repository request actions which might include triggering jobs, transformations, or observer events.

The controller is thin, simply passing request data into the service and invoking the service methods and receiving the response and then returning a consistently shaped verbose message.

## Installation

Via Composer

``` bash
$ composer require aptimumio/groovyservicepattern
```

NOTE: This is a private repository.  To require this into a project you will have to set up basic http authentication or an SSH key.

## Usage

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email jacques@aptimum.io instead of using the issue tracker.

## Credits

- [Aptimum IO][link-author]
- [All Contributors][link-contributors]

## License

Proprietary. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/aptimumio/groovyservicepattern.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/aptimumio/groovyservicepattern.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/aptimumio/groovyservicepattern/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/aptimumio/groovyservicepattern
[link-downloads]: https://packagist.org/packages/aptimumio/groovyservicepattern
[link-travis]: https://travis-ci.org/aptimumio/groovyservicepattern
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/aptimumio
[link-contributors]: ../../contributors
