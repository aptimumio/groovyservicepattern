<?php

function handle_data_object($data){
    if (is_array($data)) {
        return $data;
    }
    if($data instanceof stdClass){
        return (array) $data;
    }
    if (is_object($data) && method_exists($data, 'toArray')) {
        return $data->toArray();
    }
    return $data;
}
