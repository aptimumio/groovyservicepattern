<?php

namespace Aptimumio\GroovyServicePattern\Bases;

use Aptimumio\GroovyServicePattern\Bases\Interfaces\BaseServiceInterface;
use Aptimumio\GroovyServicePattern\Bases\Interfaces\MessagesInterface;
use Aptimumio\GroovyServicePattern\Bases\Interfaces\InputDataInterface;

use Aptimumio\GroovyServicePattern\Bases\BaseRepository;
use Aptimumio\GroovyServicePattern\Traits\MessagesTrait;
use Aptimumio\GroovyServicePattern\Traits\DebuggerTrait;
use Aptimumio\GroovyServicePattern\Traits\ActionHooks;
use Aptimumio\GroovyServicePattern\Traits\InputDataTrait;

class BaseService implements BaseServiceInterface, MessagesInterface, InputDataInterface
{
    use MessagesTrait;
    use DebuggerTrait;
    use ActionHooks;
    use InputDataTrait;

    /**
     * Holds request data
     * @var array
     */
    protected array $input_data = [];

    protected BaseRepository $repository;

    public function __construct()
    {
        $this->initMessages();
        $this->initActionHooks('handleInstanceError');
    }

    /**
     * Overrides the trait so we can pass the data along to the repo when set.
     * @param array $data
     * @return $this|mixed
     */
    public function setInputData($data)
    {
        $this->input_data = $data;
        $this->repository->setInputData($data);
        return $this;
    }


    public function getMany()
    {
        $this->runActionHooks($this->input_data, [], $this->action_hook_event_types['retrieve'], false);
        $collection = $this->repository->getMany();
        $this->mergeMessageBag($this->repository);
        if ($this->handleFalseAsError($collection, $this->repository)) {
            return false;
        }
        $this->runActionHooks($this->input_data, $collection, $this->action_hook_event_types['retrieve'], true);
        return $collection;
    }


    public function find(int $id)
    {
        $this->repository->filter(['and:id:=:' . $id]);
        return $this->getOne();
    }


    public function getOne()
    {
        $this->runActionHooks($this->input_data, [], $this->action_hook_event_types['retrieve'], false);
        $object = $this->repository->getOne();
        $this->mergeMessageBag($this->repository);
        if ($this->handleFalseAsError($object, $this->repository)) {
            return false;
        }
        $this->runActionHooks($this->input_data, $object, $this->action_hook_event_types['retrieve'], true);
        return $object;
    }


    public function create($data)
    {
        $this->repository->begin();
        $this->runActionHooks($data, [], $this->action_hook_event_types['create'], false);
        $object = $this->repository->create($data);
        $this->mergeMessageBag($this->repository);
        if ($this->handleFalseAsError($object, $this->repository)) {
            $this->repository->rollback();
            return false;
        }
        $this->runActionHooks($data, $object, $this->action_hook_event_types['create'], true);
        if ($this->hasMessages()) {
            $this->repository->rollback();
            return false;
        }
        $this->repository->commit();
        return $object;
    }


    public function update($id, $data)
    {
        $this->repository->begin();
        $this->runActionHooks($data, $id, $this->action_hook_event_types['update'], false);
        $object = $this->repository->update($id, $data);
        $this->mergeMessageBag($this->repository);
        if ($this->handleFalseAsError($object, $this->repository)) {
            $this->repository->rollback();
            return false;
        }
        $this->runActionHooks($input_data, $object, $this->action_hook_event_types['update'], true);
        if ($this->hasMessages()) {
            $this->repository->rollback();
            return false;
        }
        $this->repository->commit();
        return $object;
    }


    public function destroy($id, $data)
    {
        $input_data = array_merge(['id' => $id], $data);
        $this->runActionHooks($input_data, [], $this->action_hook_event_types['delete'], false);
        if ($this->hasMessages()) {
            $this->repository->rollback();
            return false;
        }
        $this->repository->destroy($id, $data);
        $this->mergeMessageBag($this->repository);
        $this->runActionHooks($input_data, null, $this->action_hook_event_types['delete'], true);
        if ($this->hasMessages()) {
            $this->repository->rollback();
            return false;
        }
        return true;
    }
}
