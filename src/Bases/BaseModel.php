<?php

namespace Aptimumio\GroovyServicePattern\Bases;

use Aptimumio\GroovyServicePattern\Bases\Interfaces\BaseModelInterface;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model implements BaseModelInterface
{
    /**
     * Expected to contain a default key in rules and messages which holds default rules/messages and then can have
     * additional keys to support specifics by action (eg. create/update/create_many).
     * @var array
     */
    protected array $validation_data = [
        'rules' => [],
        'messages' => [],
    ];


    /**
     * May be boolean or a public method name that is used to control the merge behavior.
     * @var bool
     */
    protected $merge_columns_to_filterable_columns = true;


    /**
     * May be boolean or a public method name that is used to control the merge behavior.
     * @var mixed
     */
    protected $merge_columns_to_sortable_columns = true;


    /**
     * Expected to contain a default key which holds default filters and then can have method keys
     * to support unique filters for joins.
     * @var array
     */
    protected array $filterable_columns = [];


    /**
     * Expected to contain a default key which holds default sorts and then can have method keys
     * to support unique sorts for joins.
     * @var array
     */
    protected array $sortable_columns = [];


    public function getValidationRules($type = 'default')
    {
        return data_get($this->validation_data, "rules.$type", data_get($this->validation_data, 'rules.default', []));
    }


    public function getValidationMessages($type = 'default')
    {
        return data_get($this->validation_data, "messages.$type", data_get($this->validation_data, 'messages.default', []));
    }


    public function getFilterableColumns($type = 'default')
    {
        $filterable_columns = data_get($this->filterable_columns, $type, data_get($this->filterable_columns, 'default', []));
        if ($type !== 'default' || !$this->merge_columns_to_filterable_columns) {
            return $filterable_columns;
        }

        return $this->getSortableOrFilterableColumns($filterable_columns, $this->merge_columns_to_filterable_columns);
    }


    public function getSortableColumns($type = 'default')
    {
        $sortable_columns = data_get($this->sortable_columns, $type, data_get($this->sortable_columns, 'default', []));
        if ($type !== 'default' || !$this->merge_columns_to_sortable_columns) {
            return $sortable_columns;
        }

        return $this->getSortableOrFilterableColumns($sortable_columns, $this->merge_columns_to_sortable_columns);
    }


    protected function getSortableOrFilterableColumns($filterable_or_sortable_columns, $merge_columns_value)
    {
        $columns = array_keys($this->getAttributes());
        if (!is_string($merge_columns_value)) {
            return array_merge($columns, $filterable_or_sortable_columns);
        }
        return $this->$merge_columns_value($columns, $filterable_or_sortable_columns);
    }
}
