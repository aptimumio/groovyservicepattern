<?php

namespace Aptimumio\GroovyServicePattern\Bases\AbstractClasses;

use Aptimumio\GroovyServicePattern\Bases\Interfaces\BaseMessageBagInterface;

use Illuminate\Support\Str;
use Illuminate\Support\MessageBag;

abstract class BaseMessageBagAbstract extends MessageBag implements BaseMessageBagInterface
{
    protected $warning_key = 'warning_';


    protected $meta_key = 'meta_';


    public function addWarningMessage($key, $message)
    {
        $key = $this->warning_key . $key;
        $this->add($key, $message);
    }


    public function addMetaMessage($key, $value)
    {
        $key = $this->meta_key . $key;
        $this->add($key, $value);
    }


    public function getWarningMessages($simplify_messages = false, $remove_prefix = false)
    {
        return $this->getMessagesByPrefix($this->warning_key, $simplify_messages, $remove_prefix);
    }


    public function getMetaMessages($simplify_messages = false, $remove_prefix = false)
    {
        return $this->getMessagesByPrefix($this->meta_key, $simplify_messages, $remove_prefix);
    }

    public function mergeMessages($message_bag){
        $messages = $message_bag->messages();
        foreach($messages as $key => $keyed_messages){
            if(is_array($keyed_messages)){
                foreach($keyed_messages as $message){
                    $this->addUnique($key, $message);
                }
            } else {
                $message = $keyed_messages;
                $this->addUnique($key, $message);
            }
        }
    }


    /**
     * Returns messages not otherwise tagged as meta or warning.
     * @return array
     */
    public function getErrorMessages()
    {
        $keys = [$this->meta_key, $this->warning_key];
        return $this->getMessagesExceptWildcardKeys($keys);
    }


    /**
     * Returns only messages that have specific prefix keys.
     * @return array
     */
    public function getKeyedMessages()
    {
        return [
            'warnings' => $this->getWarningMessages(),
            'meta' => $this->getMetaMessages()
        ];
    }


    /**
     * Returns messages grouped by their type.
     * @param bool $exclude_empty_elements
     * @return array
     */
    public function getMessagesGrouped($exclude_empty_elements = true)
    {
        $messages = [
            'warnings' => $this->getWarningMessages(),
            'meta' => $this->getMetaMessages(),
            'errors' => $this->getErrorMessages(),
        ];
        if ($exclude_empty_elements) {
            foreach (array_keys($messages) as $key) {
                if (empty($messages[$key])) {
                    unset($messages[$key]);
                }
            }
        }
        return $messages;
    }


    /**
     * Return messages by prefix modified according to booleans.
     * @param string $prefix
     * @param $simplify_messages
     * @param $remove_prefix
     * @return array|array[]
     */
    protected function getMessagesByPrefix(string $prefix, $simplify_messages, $remove_prefix)
    {
        $messages = $this->getMessagesForWildcardKey($prefix);
        if ($simplify_messages) {
            $messages = $this->simplifyMessages($messages);
        }

        return !$remove_prefix
            ? $messages
            : $this->removePrefix($prefix, $messages);
    }


    /**
     * If the message array is an array of one, return just the message as a string.
     * @param $messages
     * @return array|array[]
     */
    protected function simplifyMessages($messages)
    {
        return array_map(function ($message) {
            return is_array($message) && count($message) === 1 ? array_shift($message) : $message;
        }, $messages);
    }


    /**
     * @param $prefix
     * @param $messages
     * @return array
     */
    protected function removePrefix($prefix, $messages)
    {
        $keys = array_keys($messages);
        $new_keys = array_map(function ($key) use ($prefix) {
            return str_replace($prefix, '', $key);
        }, $keys);
        return array_combine($new_keys, $messages);
    }


    /**
     * Overrides existing method that doesn't work the way I would expect.  As originally written is more like
     * get for exact key.
     * @param string $key
     * @param string $format
     * @return array
     */
    protected function getMessagesForWildcardKey($key, $format = ':message')
    {
        return collect($this->messages)
            ->filter(function ($messages, $messageKey) use ($key) {
                return Str::contains($messageKey, $key);
            })->all();
    }


    /**
     * Returns all messages not containing any of the keys in the input array
     * @param array $keys
     * @return array
     */
    protected function getMessagesExceptWildcardKeys(array $keys): array
    {
        return collect($this->messages)
            ->filter(function ($messages, $messageKey) use ($keys) {
                return array_reduce($keys, function ($acc, $key) use ($messageKey) {
                    if (!$acc) {
                        return $acc;
                    }
                    if (Str::contains($messageKey, $key)) {
                        return false;
                    }
                    return $acc;
                }, true);
            })->all();
    }


    /**
     * @param $key
     * @param $message
     * @return bool
     */
    protected function addUnique($key, $message){
        if ($this->isUnique($key, $message)) {
            $this->add($key, $message);
        }
        return true;
    }
}
