<?php

namespace Aptimumio\GroovyServicePattern\Bases;

use Aptimumio\GroovyServicePattern\Bases\AbstractClasses\BaseMessageBagAbstract;

class BaseMessageBag extends BaseMessageBagAbstract
{
    // See abstract for concrete methods
}
