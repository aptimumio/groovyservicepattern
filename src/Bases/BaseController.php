<?php

namespace Aptimumio\GroovyServicePattern\Bases;

use Aptimumio\GroovyServicePattern\Bases\Interfaces\BaseServiceInterface;
use Aptimumio\GroovyServicePattern\Bases\Interfaces\BaseControllerInterface;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Aptimumio\GroovyServicePattern\Traits\ResponseSenderTrait;
use Aptimumio\GroovyServicePattern\Traits\MessagesTrait;
use Aptimumio\GroovyServicePattern\Traits\DebuggerTrait;

class BaseController implements BaseControllerInterface
{
    use ResponseSenderTrait;
    use MessagesTrait;
    use DebuggerTrait;

    /**
     * Holds the service layer class
     * @var
     */
    protected BaseServiceInterface $service;


    public function __construct()
    {
        $this->initMessages();
    }

    public function index(Request $request)
    {
        $entities = $this->service->setInputData($request->all())->getMany();
        $this->mergeMessageBag($this->service);
        return $this->sendResponse($entities, $this->message_bag->getMessagesGrouped());
    }


    public function find(Request $request)
    {
        $entity = $this->service->setInputData($request->all())->getOne();
        $this->mergeMessageBag($this->service);
        return $this->sendResponse($entity, $this->message_bag->getMessagesGrouped());
    }


    public function show(Request $request, $id)
    {
        $entity = $this->service->setInputData($request->all())->find($id);
        $this->mergeMessageBag($this->service);
        return $this->sendResponse($entity, $this->message_bag->getMessagesGrouped());
    }


    public function store(Request $request)
    {
        $entity = $this->service->create($request->all());
        $this->mergeMessageBag($this->service);
        return $this->sendResponse($entity, $this->message_bag->getMessagesGrouped());
    }


    public function update(Request $request, $id)
    {
        $entity = $this->service->update($id, $request->all());
        $this->mergeMessageBag($this->service);
        return $this->sendResponse($entity, $this->message_bag->getMessagesGrouped());
    }


    public function destroy(Request $request, $id)
    {
        $this->service->destroy($id, $request->all());
        $this->mergeMessageBag($this->service);
        return $this->sendResponse([], $this->message_bag->getMessagesGrouped());
    }
}
