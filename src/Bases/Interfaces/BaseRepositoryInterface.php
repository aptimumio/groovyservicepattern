<?php


namespace Aptimumio\GroovyServicePattern\Bases\Interfaces;


interface BaseRepositoryInterface
{
    /**
     * Resets all query params and constraints to their defaults.
     * @return mixed
     */
    public function reset();


    /**
     * Gets the name of the model associated with this repository.
     * @return mixed
     */
    public function getModelName();


    /******************
     * Crud Operations And Collection/Object handling.
     ******************/


    /**
     * Returns a collection.
     * @return mixed
     */
    public function getMany();

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id);

    /**
     * Returns a single object.
     * @return mixed
     */
    public function getOne();

    /**
     * Creates an object.
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * Updates an object.
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id, array $data);

    /**
     * Destroys an object.
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function destroy(int $id, array $data);


    /******************
     * Builder methods
     ******************/


    /**
     * Sets pagination on or off and how many items per page.
     * @param bool $state
     * @param int $per_page
     * @return mixed
     */
    public function paginate(bool $state, int $per_page);


    /**
     * Limits the returned collection size.
     * @param int $limit
     * @return mixed
     */
    public function limit(int $limit);


    /**
     * Specifies the starting offset for a collection.
     * @param int $offset
     * @return mixed
     */
    public function offset(int $offset);


    /**
     * Applies a filter or set of filters.
     * @param array $filter
     * @return mixed
     */
    public function filter(array $filter);


    /**
     * Applies an order by clause (or multiples depending on your implementation).
     * @param string $order
     * @return mixed
     */
    public function orderBy(string $order);


    /**
     * Array of eager loads to be applied to the model.
     * @param array $eager_loads
     * @return mixed
     */
    public function eagerLoads(array $eager_loads);



    /******************
     * Transaction methods
     ******************/


    /**
     * Convenience method for transaction begin.
     * Especially useful when using a split read/write, partitioned, or sharded connections.
     * @return mixed
     */
    public function begin();

    /**
     * Convenience method for transaction rollback.
     * Especially useful when using a split read/write, partitioned, or sharded connections.
     * @return mixed
     */
    public function rollback();

    /**
     * Convenience method for transaction commit.
     * Especially useful when using a split read/write, partitioned, or sharded connections.
     * @return mixed
     */
    public function commit();


}
