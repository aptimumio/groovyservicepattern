<?php

namespace Aptimumio\GroovyServicePattern\Bases\Interfaces;

interface MessagesInterface
{
    /**
     * returns all messages
     * @return mixed
     */
    public function getMessages();

    /**
     * Returns the message bag object
     * @return mixed
     */
    public function getMessageBag();
}
