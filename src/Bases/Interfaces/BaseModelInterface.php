<?php

namespace Aptimumio\GroovyServicePattern\Bases\Interfaces;

/**
 * Interface BaseModelInterface
 * Adds methods for consistent handling of common issues regarding validation, filtering, and sorting.
 * @package Aptimumio\GroovyServicePattern\Bases
 */
interface BaseModelInterface
{
    /**
     * Returns the validation rules for the given type where type is a pointer to the collection.
     * @param string $type
     * @return array|mixed
     */
    public function getValidationRules(string $type);


    /**
     * Returns the validation messages for the given type where type is a pointer to the collection.
     * @param string $type
     * @return mixed
     */
    public function getValidationMessages(string $type);


    /**
     * Returns the filterable columns for the given type where type is a pointer to the collection.
     * @param string $type
     * @return mixed
     */
    public function getFilterableColumns(string $type);


    /**
     * Returns the sortable columns for the given type where type is a pointer to the collection.
     * @param string $type
     * @return mixed
     */
    public function getSortableColumns(string $type);
}
