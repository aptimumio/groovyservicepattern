<?php

namespace Aptimumio\GroovyServicePattern\Bases\Interfaces;


interface BaseServiceInterface
{

    /**
     * Proxies requests with the expectation of a collection return through to a repository
     * allowing for before and after actions, validations, etc.
     * @return mixed
     */
    public function getMany();


    /**
     * Proxies requests with the expectation of an object return through to a repository
     * allowing for before and after actions, validations, etc.
     * @param int $id
     * @return mixed
     */
    public function find(int $id);

    /**
     * Proxies requests with the expectation of an object return through to a repository
     * allowing for before and after actions, validations, etc.
     * @return mixed
     */
    public function getOne();


    /**
     * Proxies requests with the expectation of creating an object through to a repository
     * allowing for before and after actions, validations, etc.
     * @param array $data
     * @return mixed
     */
    public function create(array $data);


    /**
     * Proxies requests with the expectation of updating an object through to a repository
     * allowing for before and after actions, validations, etc.
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id, array $data);


    /**
     * Proxies requests with the expectation of deleting an object through to a repository
     * allowing for before and after actions, validations, etc.
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function destroy(int $id, array $data);
}
