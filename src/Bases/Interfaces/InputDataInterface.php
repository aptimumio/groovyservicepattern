<?php

namespace Aptimumio\GroovyServicePattern\Bases\Interfaces;


interface InputDataInterface
{
    /**
     * Sets input data to be used instead of plucking data out of requests to eliminate variability.
     * @param array $data
     * @return mixed
     */
    public function setInputData(array $data);

    /**
     * Gets stored input data.
     * @return mixed
     */
    public function getInputData();
}
