<?php


namespace Aptimumio\GroovyServicePattern\Bases\Interfaces;


interface BaseMessageBagInterface
{
    /**
     * Internally adds a warning prefix to the key.
     * @param string $key
     * @param string $message
     * @return mixed
     */
    public function addWarningMessage(string $key, string $message);

    /**
     * Internally adds a meta prefix to the key.
     * @param string $key
     * @param string $message
     * @return mixed
     */
    public function addMetaMessage(string $key, string $message);

    /**
     * Returns messages prefixed by warning.
     * @return mixed
     */
    public function getWarningMessages();

    /**
     * Returns messages prefixed by meta.
     * @return mixed
     */
    public function getMetaMessages();
}
