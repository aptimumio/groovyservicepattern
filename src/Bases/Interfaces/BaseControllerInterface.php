<?php

namespace Aptimumio\GroovyServicePattern\Bases\Interfaces;

use Illuminate\Http\Request;

interface BaseControllerInterface
{
    /**
     * Standard rest endpoint
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request);


    /**
     * Non Standard rest endpoint when your expectation is to filter and sort and get an object instead of a collection.
     * This could be achieved by placing a limit on index; included for clarity.
     * @param Request $request
     * @return mixed
     */
    public function find(Request $request);


    /**
     * Standard rest endpoint
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function show(Request $request, int $id);


    /**
     * Standard rest endpoint
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request);


    /**
     * Standard rest endpoint
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, int $id);


    /**
     * Standard rest endpoint
     * Use with caution, prefer soft deletes in most cases.
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function destroy(Request $request, int $id);
}
