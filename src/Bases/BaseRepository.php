<?php

namespace Aptimumio\GroovyServicePattern\Bases;

use Aptimumio\GroovyServicePattern\Bases\Interfaces\BaseRepositoryInterface;
use Aptimumio\GroovyServicePattern\Bases\Interfaces\MessagesInterface;
use Aptimumio\GroovyServicePattern\Bases\Interfaces\InputDataInterface;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Aptimumio\GroovyServicePattern\Traits\MessagesTrait;
use Aptimumio\GroovyServicePattern\Traits\DebuggerTrait;
use Aptimumio\GroovyServicePattern\Traits\ActionHooks;
use Illuminate\Support\Facades\Validator;
use Aptimumio\GroovyServicePattern\Validators\OrderByValidator;
use Aptimumio\GroovyServicePattern\Validators\FiltersValidator;
use Aptimumio\GroovyServicePattern\Traits\InputDataTrait;

class BaseRepository implements BaseRepositoryInterface, MessagesInterface, InputDataInterface
{
    use MessagesTrait;
    use DebuggerTrait;
    use ActionHooks;
    use InputDataTrait;

    /**
     * @var Model
     */
    protected Model $model;

    protected bool $debug = false;

    protected object $query;

    protected bool $is_paginated = false;

    protected string $default_order_by = 'id:asc';

    protected string $order_by = '';

    protected array $default_eager_loads = [];

    protected array $eager_loads = [];

    protected int $default_limit = 100;

    protected int $limit = 100;

    protected int $offset = 0;

    protected int $per_page = 100;


    /**
     * @var string[]
     */
    protected array $messages = [
        'limit_warning' => 'There are :count records in this query but only :limit records are returned due to the current limit.'
    ];

    public function __construct()
    {
        $this->initMessages();
        $this->initActionHooks('handleInstanceError');

        // Priority is 100 so other hooks registered before unless specifically scheduled after the validation will occur before.  eg. transforms.
        $action_types = [$this->action_hook_event_types['create'], $this->action_hook_event_types['update']];
        $this->registerBeforeEventHook($this, 'isValid', $action_types, 100);
    }


    public function reset()
    {
        $this->stale_input_data[] = $this->input_data;
        $this->input_data = [];
        $this->initMessages();
        return $this;
    }


    public function getModelName()
    {
        $temp = explode("\\", get_class($this->getModel()));
        return array_pop($temp);
    }


    /******************
     * Crud Operations And Collection/Object handling.
     ******************/


    public function getMany()
    {
        $input_data = [];
        $collection = $this->applyClauses()->getCollection();
        $this->runActionHooks($input_data, $collection, $this->action_hook_event_types['retrieve'], true);
        return $collection;
    }


    public function find(int $id)
    {
        if (!$id) {
            $this->message_bag->add('validation', 'Find operations require an id.');
            return false;
        }
        $this->filter(['and:id:=:' . $id]);
        return $this->getOne();
    }


    public function getOne()
    {
        $input_data = [];
        $object = $this->applyClauses()->first();
        $this->runActionHooks($input_data, $object, $this->action_hook_event_types['retrieve'], true);
        return $object;
    }


    public function create($data)
    {
        $this->runActionHooks($data, $this->getModel(), $this->action_hook_event_types['create'], false);
        if ($this->hasMessages()) {
            return false;
        }
        $this->debugLog(true);
        try {
            $object = $this->getModel()->create($data);
        } catch (\Exception $exception) {
            $model_name = $this->getModelName();
            $this->addExceptionError($exception, "An error occurred while creating a/an $model_name");
            return false;
        }
        $this->debugLogToMessages();
        $this->debugLog(false);
        $this->runActionHooks($data, $object, $this->action_hook_event_types['create'], true);
        if ($this->hasMessages()) {
            return false;
        }
        return $object;
    }


    public function update($id, $data)
    {
        $this->runActionHooks($data, $id, $this->action_hook_event_types['update'], false);
        if ($this->hasMessages()) {
            return false;
        }
        $this->debugLog(true);
        try {
            $update_data = array_merge($data, ['id' => $id]);
            $this->getModel()->where('id', $id)->update($update_data);
            $object = $this->find($id);
        } catch (\Exception $exception) {
            $model_name = $this->getModelName();
            $this->addExceptionError($exception, "An error occurred while updating a/an $model_name");
            return false;
        }
        $this->debugLogToMessages();
        $this->debugLog(false);
        $this->runActionHooks($data, $object, $this->action_hook_event_types['update'], true);
        if ($this->hasMessages()) {
            return false;
        }
        return $object;
    }


    public function destroy($id, $data)
    {
        $this->runActionHooks($data, $this->getModel(), $this->action_hook_event_types['delete'], false);
        if ($this->hasMessages()) {
            return false;
        }
        $this->debugLog(true);
        try {
            $object = $this->getOne($id);
            $object->delete();
            $data['id'] = $id;
        } catch (\Exception $exception) {
            $model_name = $this->getModelName();
            $this->addExceptionError($exception, "An error occurred while creating a/an $model_name");
            return false;
        }
        $this->debugLogToMessages();
        $this->debugLog(false);
        $this->runActionHooks($data, [], $this->action_hook_event_types['delete'], true);
        if ($this->hasMessages()) {
            return false;
        }
        return [];
    }


    /******************
     * Builder methods
     ******************/


    public function paginate(bool $state, int $per_page = 100)
    {
        $this->input_data = array_merge($this->input_data, [
            'is_paginated' => $state,
            'per_page' => $per_page
        ]);
        return $this;
    }


    public function limit($limit)
    {
        $this->input_data['limit'] = $limit;
        return $this;
    }


    public function offset($offset)
    {
        $this->input_data['offset'] = $offset;
        return $this;
    }


    public function filter($filter)
    {
        $this->input_data['filter'] = $filter;
        return $this;
    }


    public function orderBy($order)
    {
        $this->input_data['orderBy'] = $order;
        return $this;
    }


    public function eagerLoads(array $eager_loads)
    {
        $this->eager_loads = $eager_loads;
        return $this;
    }


    public function debug(bool $state)
    {
        $this->debug = $state;
        return $this;
    }


    /******************
     * Transaction methods
     ******************/


    public function begin()
    {
        DB::beginTransaction();
    }


    public function rollback()
    {
        DB::rollback();
    }


    public function commit()
    {
        DB::commit();
    }

    public function debugLog(bool $state)
    {
        if (!$this->debug) {
            return;
        }
        if ($state) {
            DB::enableQueryLog();
        } else {
            DB::disableQueryLog();
        }

    }

    public function debugLogToMessages()
    {
        if ($this->debug) {
            $query_log = DB::getQueryLog();
            $this->message_bag->addMetaMessage('query_log', json_encode($query_log));
        }
    }


    /**
     * Validator.  Only public because it is required to be when used through the actions trait.
     * @param $data - always passed to registered handlers
     * @param $object - always passed to registered handlers
     * @param $action_type
     * @return bool
     */
    protected function isValid($data, $object, $action_type = 'default')
    {
        $rules = $this->getModel()->getValidationRules($action_type);
        $messages = $this->getModel()->getValidationMessages($action_type);
        $validator = Validator::make($data, $rules, $messages);
        if ($validator->fails()) {
            $this->mergeMessageBag($validator->getMessageBag());
            return false;
        }
        return $data;
    }

    /**
     * @param bool $query
     * @return mixed
     */
    protected function getCollection($query = false)
    {
        $query = $query ? $query : $this->query;
        if ($this->is_paginated) {
            return $query->paginate($this->per_page);
        }
        $count = $query->count();
        if ($count > $this->limit) {
            $limit = $this->limit;
            $message = trans($this->messages['limit_warning'], ['count' => $count, 'limit' => $limit]);
            $this->getMessageBag()->addWarningMessage('limit', $message);
            $pages = ceil($count / $this->limit);
            $page = floor($this->offset / $this->limit);
            $this->getMessageBag()->addMetaMessage('page', $page);
            $this->getMessageBag()->addMetaMessage('per_page', $this->limit);
            $this->getMessageBag()->addMetaMessage('pages', $pages);
        }
        $this->debugLog(true);
        $collection = $query->limit($this->limit)->offset($this->offset)->get();
        $this->debugLogToMessages();
        $this->debugLog(false);
        return $collection;
    }

    protected function first()
    {
        $this->debugLog(true);
        $object = $this->query->first();
        $this->debugLogToMessages();
        $this->debugLog(false);
        return $object;
    }

    /**
     *  Sets the discrete properties on the class for simplicity.  This is invoked by getModel.
     */
    protected function setQueryConstraints()
    {
        $data = $this->getInputData();
        $this->is_paginated = data_get($data, 'is_paginated', false);
        $this->per_page = data_get($data, 'per_page', $this->per_page);
        $this->limit = data_get($data, 'limit', $this->default_limit);
        $this->offset = data_get($data, 'offset', 0);
        $this->order_by = data_get($data, 'order_by', $this->default_order_by);
        $this->eager_loads = data_get($data, 'eager_loads', $this->default_eager_loads);
        $this->debug = data_get($data, 'debug', false);
    }

    /**
     * @return $this
     */
    protected function applyClauses()
    {
        $self = $this;
        $this->query = $this->getModel()
            ->with($this->eager_loads)
            ->where(function ($query) use ($self) {
                return $self->getWhere($query);
            });
        $this->query = $this->getOrderBy();
        return $this;
    }

    /**
     * @param $query
     * @return mixed
     */
    protected function getWhere($query)
    {
        $filters_validator = app()->make(FiltersValidator::class);
        $filters = data_get($this->input_data, 'filter', false);
        if (!$filters) {
            return $query;
        }
        $allowed_filters = $this->getModel()->getFilterableColumns();
        $filters = $filters_validator->isValid($filters, $allowed_filters);
        $this->mergeMessageBag($filters_validator);
        if (!$filters) {
            return $query;
        }

        return $this->parseFilter($query, $filters);
    }

    /**
     * @return object
     */
    protected function getOrderBy()
    {
        $order_by_validator = app()->make(OrderByValidator::class);

        $order_by = !empty($this->order_by) ? $this->order_by : $this->default_order_by;

        $allowed_columns = $this->getModel()->getSortableColumns('default');
        $order_by = $order_by_validator->isValid($order_by, $allowed_columns);
        $this->mergeMessageBag($order_by_validator);

        if (!$order_by) {
            return $this->query;
        }

        $order_by_phrases = explode('|', $order_by);
        $query = $this->query;
        foreach ($order_by_phrases as $phrase) {
            list($column, $direction) = explode(':', $phrase);
            $query->orderBy($column, $direction);
        }
        return $query;
    }

    /**
     * Where builder method.
     * @param $query
     * @param array $filter
     * @return mixed
     */
    protected function parseFilter($query, $filter = [])
    {
        $groups = $this->prepareGroups($filter);
        return $this->parseGroups($query, $groups);
    }

    /**
     * Where builder method
     * @param $filter
     * @return mixed
     */
    protected function prepareGroups($filter)
    {
        foreach ($filter as $key => $item) {
            if (is_array($item)) {
                $groups[] = $this->prepareGroups($item);
            }
            if (is_string($item)) {
                $simple_filter = $this->parsePhrase($item);
                $groups[] = $simple_filter;
                $simple_filter = null;
            }
        }
        return $groups;
    }

    /**
     * Where builder method
     * @param $filter
     * @return mixed
     */
    protected function parsePhrase($filter)
    {
        $filter_data = explode(':', $filter);

        $simple_filter['function'] = $filter_data[0] === 'and' ? 'where' : 'orWhere';
        $simple_filter['field'] = $filter_data[1];
        $simple_filter['operator'] = $filter_data[2];
        $simple_filter['values'] = $filter_data[3];

        return $simple_filter;
    }

    /**
     * Where builder method
     * @param $query
     * @param $groups
     * @return mixed
     */
    protected function parseGroups(&$query, $groups)
    {
        foreach ($groups as $operator => $group) {
            if (!array_key_exists('function', $group)) {
                $closure = function ($query) use ($group) {
                    $this->parseGroups($query, $group);
                };
                $query->where($closure);
            } else {
                $query->{$group['function']}($group['field'], $group['operator'], $group['values']);
            }
        }
        return $query;
    }

    /**
     * @return Model
     */
    protected function getModel()
    {
        $this->setQueryConstraints();
        return $this->model;
    }
}
