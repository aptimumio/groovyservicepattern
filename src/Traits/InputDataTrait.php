<?php

namespace Aptimumio\GroovyServicePattern\Traits;

trait InputDataTrait
{
    protected array $input_data = [];


    protected array $stale_input_data = [];


    public function setInputData(array $data)
    {
        $this->input_data = $data;
    }


    public function getInputData()
    {
        return $this->input_data;
    }
}
