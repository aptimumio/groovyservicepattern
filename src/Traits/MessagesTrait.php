<?php

namespace Aptimumio\GroovyServicePattern\Traits;

use Aptimumio\GroovyServicePattern\Bases\BaseMessageBag;

trait MessagesTrait
{
    /**
     * Extended laravel message bag populated with instance when initialized.
     * @var
     */
    protected BaseMessageBag $message_bag;

    /**
     * Provides access to a message bag from other contexts and simplified local entry point.
     * @return BaseMessageBag
     */
    public function getMessageBag()
    {
        return $this->message_bag;
    }


    public function getMessages()
    {
        return $this->message_bag->messages();
    }


    /**
     * Helper method - could be accessed through getMessageBag as well.
     * @return bool
     */
    public function hasMessages()
    {
        return $this->message_bag->isNotEmpty();
    }


    /**
     * @return $this
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function initMessages()
    {
        $this->message_bag = app()->make(BaseMessageBag::class);
        return $this;
    }


    /**
     * Merges either a message bag object or the message bag on an object that has a getMessageBag method.
     * @param $message_bag_or_instance
     * @return $this|bool
     */
    protected function mergeMessageBag($message_bag_or_instance)
    {
        $message_bag = $message_bag_or_instance;
        $is_message_bag = $message_bag_or_instance instanceof BaseMessageBag;
        if (!$is_message_bag) {
            if (!method_exists($message_bag_or_instance, 'getMessageBag')) {
                $message = 'Attempted to merge message bag from an object without a message bag!' . get_class($message_bag_or_instance);
                $this->message_bag->add('internal_error', $message);
                return false;
            }
            $message_bag = $message_bag_or_instance->getMessageBag();
        }
        $this->message_bag->mergeMessages($message_bag);
        return $this;
    }


    /**
     * Helper method to self document the trapping of false on return when we expect an object or collection.
     * @param $object_or_collection
     * @param $instance
     * @return bool
     */
    protected function handleFalseAsError($object_or_collection, $instance)
    {
        if ($object_or_collection === false) {
            $this->handleInstanceError($instance);
            return true;
        }
        return false;
    }


    /**
     * @param $instance
     */
    protected function handleInstanceError($instance)
    {
        $current_class = get_class($this);
        $is_of_current_class = $instance instanceof $current_class;
        if (!$is_of_current_class) {
            $this->mergeMessageBag($instance);
        }
    }


    /**
     * @param \Exception $exception
     * @param $message
     */
    protected function addExceptionError(\Exception $exception, $message)
    {
        $message .= '(' . $exception->getMessage() . ' [' . $exception->getCode() . '])';
        $this->getMessageBag()->add('exception', $message);
    }
}
