<?php

namespace Aptimumio\GroovyServicePattern\Traits;

trait ActionHooks
{
    /**
     * Action Hooks allow us to modify data at a given point in time.
     * Any registered action hook should return false if the intent is to stop execution.
     * Otherwise it should return the data it is operating on.  For example:
     *
     * BeforeEvent hooks will usually be operating on input data (or be evaluating other true false conditions)
     * and should return the input data if not returning false.
     *
     * AfterEvent hooks will usually be operating on the result of a data base operation so are receiving
     * data objects representing a database entity or collection which ultimately will be returned to the caller.
     * False will interrupt execution.  Otherwise, return the modified data object.
     * @var array
     */
    protected $registered_action_hooks = [];

    /**
     * @var bool
     */
    protected $instance_error_handler_method = false;

    /**
     * This is a standard list of event types matching Laravel's observers.  Others can be included just as strings
     * input to the action_types argument of the register methods.
     * @var string[]
     */
    protected $action_hook_event_types = [
        'retrieve' => 'retrieve',
        'create' => 'create',
        'update' => 'update',
        'delete' => 'delete',
        'restore' => 'restore'
    ];

    /**
     * Name of method in class that will handle instance errors propagating errors to local message bag.
     * @param $instance_error_handler_method
     */
    public function initActionHooks($instance_error_handler_method){
        $this->instance_error_handler_method = $instance_error_handler_method;
    }

    /**
     * @param $class_instance
     * @param $method
     * @param mixed $action_types
     * @param bool $is_after
     * @param int $priority - determines the execution order
     */
    public function registerActionHook($class_instance, $method, $action_types, $is_after = true, $priority = 1){
        $this->registered_action_hooks[] = (object) [
            'instance' => $class_instance,
            'method' => $method,
            'action_types' => is_string($action_types) ? [$action_types] : $action_types,
            'is_after' => $is_after,
            'priority' => $priority,
        ];
    }

    /**
     * See registerActionHook for details on what a hook MUST do.
     * @param $class_instance
     * @param $method
     * @param $action_types
     * @param $priority
     */
    public function registerBeforeEventHook($class_instance, $method, $action_types, $priority){
        $this->registerActionHook($class_instance, $method, $action_types, false, $priority);
    }

    /**
     * See registerActionHook for details on what a hook MUST do.
     * @param $class_instance
     * @param $method
     * @param $action_types
     * @param $priority
     */
    public function registerAfterEventHook($class_instance, $method, $action_types, $priority){
        $this->registerActionHook($class_instance, $method, $action_types, true, $priority);
    }

    /**
     * @param $input_data
     * @param $object_or_collection
     * @param $action_type
     * @param boolean $is_after
     */
    protected function runActionHooks(&$input_data, $object_or_collection, $action_type, $is_after){
        $valid_hooks_sorted = $this->getRegisteredActionHooks($action_type, $is_after);
        foreach($valid_hooks_sorted as $action){
            if(in_array($action_type, $action->action_types) && $action->is_after === $is_after){
                $instance = $action->instance;
                $method = $action->method;
                $result = $instance->$method($input_data, $object_or_collection, $action_type);
                if(!$result){
                    if($this->instance_error_handler_method){
                        $error_handler = $this->instance_error_handler_method;
                        $this->$error_handler($instance);
                    } else {
                        // @TODO - system log error and probably slack
                    }
                    break;
                }
                $input_data = $result;
            }
        }
    }

    /**
     * @param $action_type
     * @param $is_after
     * @return array
     */
    protected function getRegisteredActionHooks($action_type, $is_after)
    {
        // Get the ones we need
        $valid_hooks = array_filter($this->registered_action_hooks, function($hook) use ($action_type, $is_after){
           return in_array($action_type, $hook->action_types) && $hook->is_after === $is_after;
        });
        // Sort them by priority
        usort($valid_hooks, function($a, $b){
            $a_priority = data_get($a, 'priority', 1);
            $b_priority = data_get($b, 'priority', 1);
            return $a_priority > $b_priority;
        });
        return $valid_hooks;
    }
}
