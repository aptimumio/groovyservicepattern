<?php

namespace Aptimumio\GroovyServicePattern\Traits;

use Illuminate\Support\Facades\Response;

trait ResponseSenderTrait
{
    /**
     * @param $data
     * @param $extra_data
     * @param int $http_code
     * @return \Illuminate\Http\Response
     */
    protected function sendSuccess($data, $extra_data, $http_code = 200)
    {
        $response_data = array_merge([
            'data' => handle_data_object($data),
        ], $extra_data);
        return $this->getResponse($response_data, $http_code);
    }

    /**
     * @param $data - usually empty in this case but could provide some meta
     * @param $extra_data
     * @param int $http_code
     * @return \Illuminate\Http\Response
     */
    protected function sendFailure($data, $extra_data, $http_code = 422)
    {
        $response_data = array_merge([
            'data' => $data,
        ], $extra_data);
        return $this->getResponse($response_data, $http_code);
    }

    /**
     * @param $data
     * @param $extra_data - warnings, meta, and errors
     * @return \Illuminate\Http\Response
     */
    protected function sendResponse($data, $extra_data)
    {
        if ($data === false) {
            return $this->sendFailure($data, $extra_data);
        }
        return $this->sendSuccess($data, $extra_data);
    }

    /**
     * @param $data
     * @param $http_code
     * @return \Illuminate\Http\Response
     */
    protected function getResponse($data, $http_code)
    {
        return Response::make($data, $http_code);
    }
}
