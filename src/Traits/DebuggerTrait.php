<?php

namespace Aptimumio\GroovyServicePattern\Traits;

use Illuminate\Support\Facades\Log;

trait DebuggerTrait
{
    /**
     * @var bool
     */
    protected bool $debug = false;

    public function setDebug(bool $state)
    {
        $this->debug = $state;
        return $this;
    }

    protected function debugLog($data, $message = '')
    {
        return $this->handleMessageAndData($message, $data, 'logOut');
    }

    protected function debugEcho($data, $message = '')
    {
        return $this->handleMessageAndData($message, $data, 'echoOut');
    }

    protected function handleMessageAndData($message, $data, $method)
    {
        if (!$this->debug) {
            return false;
        }
        $message = !empty($message) ? $message . ': ' : $message;
        $data = handle_data_object($data);
        $data_output = is_array($data) ? print_r($data, true) : $data;
        return $this->$method($message, $data_output);
    }

    protected function logOut($message, $data)
    {
        Log::info($message . $data);
        return true;
    }

    protected function echoOut($message, $data)
    {
        echo $message . $data;
        return true;
    }
}
