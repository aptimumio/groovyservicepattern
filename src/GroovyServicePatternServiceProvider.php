<?php

namespace Aptimumio\GroovyServicePattern;

use Illuminate\Support\ServiceProvider;

class GroovyServicePatternServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'aptimumio');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'aptimumio');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        // $this->loadRoutesFrom(__DIR__.'/routes.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/groovyservicepattern.php', 'groovyservicepattern');

        // Register the service the package provides.
        $this->app->singleton('groovyservicepattern', function ($app) {
            return new GroovyServicePattern;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['groovyservicepattern'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/groovyservicepattern.php' => config_path('groovyservicepattern.php'),
        ], 'groovyservicepattern.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/aptimumio'),
        ], 'groovyservicepattern.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/aptimumio'),
        ], 'groovyservicepattern.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/aptimumio'),
        ], 'groovyservicepattern.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
