<?php

namespace Aptimumio\GroovyServicePattern\Facades;

use Illuminate\Support\Facades\Facade;

class GroovyServicePattern extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'groovyservicepattern';
    }
}
