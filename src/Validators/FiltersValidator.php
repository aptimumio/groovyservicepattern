<?php

namespace Aptimumio\GroovyServicePattern\Validators;

use Aptimumio\GroovyServicePattern\Traits\MessagesTrait;

/**
 * Validates the structure and content of a filter.  Returns false on invalid structure.  Removes invalid columns.
 * Class FiltersValidator
 * @package Aptimumio\GroovyServicePattern\Validators
 */
class FiltersValidator
{

    use MessagesTrait;

    protected $messages = [
        'invalid_type' => 'Filter must be an array of string items or include other arrays of string items.',
        'invalid_logic' => "Filters support only and/or logic.  For example: ['or:name:=:john', 'or:name:=:char']",
        'invalid_operator' => 'Filters support the following operators: ',
        'invalid_format' => 'Filter string format is invalid!  The format is: logic:column:operator:value.  For example: and:name:=:john',
    ];

    protected $valid_logic = ['AND', 'OR'];

    protected $valid_operators = ['=', '<', '<=', '>', '>=', 'IN', 'NOT IN', 'LIKE', 'NOT LIKE'];

    public function __construct()
    {
        $this->initMessages();
    }


    /**
     * @param $filters
     * @param $allowed_columns
     * @return array|bool
     */
    public function isValid($filters, $allowed_columns)
    {
        if (!is_array($filters)) {
            $this->getMessageBag()->addWarningMessage('invalid_type', $this->messages['invalid_type']);
            return false;
        }

        if (!$this->hasValidFormats($filters)) {
            $this->getMessageBag()->addWarningMessage('invalid_format', $this->messages['invalid_format']);
            return false;
        }

        return $this->getValidColumns($filters, $allowed_columns);
    }


    /**
     * Recursively inspect the filters array checking for syntax of the individual filter strings.
     * @param $filters
     * @param int $index
     * @param bool $has_valid_formats
     * @return bool
     */
    protected function hasValidFormats($filters, $index = 0, $has_valid_formats = true)
    {
        echo "\n";
        if (is_array($filters)) {
            foreach ($filters as $index => $filter) {
                $is_valid = $this->hasValidFormats($filter, $index);
                if (!$is_valid) {
                    $has_valid_formats = false;
                }
            }
            return $has_valid_formats;
        } else {
            $filter_string = $filters;
            return $this->isValidFilterString($filter_string, $index);
        }
    }


    /**
     * @param $filter_string
     * @param $index
     * @return bool
     */
    protected function isValidFilterString($filter_string, $index)
    {
        $temp = explode(':', $filter_string);
        $count_of_elements = count($temp);
        if (($count_of_elements === 3 && $index === 0) || $count_of_elements === 4) {
            $logic_index = $count_of_elements === 3 ? -1 : 0;
            $operator_index = $count_of_elements === 3 ? 1 : 2;
            if ($count_of_elements === 4) {
                $logic = strtoupper($temp[$logic_index]);
                if (!in_array($logic, $this->valid_logic)) {
                    $this->getMessageBag()->add('validation', $this->messages['invalid_logic']);
                    return false;
                }
            }
            $operator = strtoupper($temp[$operator_index]);
            if (!in_array($operator, $this->valid_operators)) {
                $message = $this->messages['invalid_operator'] . implode(',', $this->valid_operators);
                $this->getMessageBag()->add('validation', $message);
                return false;
            }
            return true;
        }
        return false;
    }


    protected function getValidColumns($filters, $allowed_columns = [])
    {
        if (is_array($filters)) {
            $remove_indexes = [];
            foreach ($filters as $index => $filter) {
                $valid_columns = $this->getValidColumns($filter, $allowed_columns);
                if ($valid_columns && is_array($valid_columns)) {
                    $filters[$index] = $valid_columns;
                }
                if (!$valid_columns) {
                    $remove_indexes = [$index];
                }
            }
            if (count($remove_indexes)) {
                for ($i = count($filters); $i > -1; $i--) {
                    if (in_array($i, $remove_indexes)) {
                        unset($filters[$i]);
                    }
                }
            }
            return $filters;
        } else {
            $filter_string = $filters;
            return $this->isValidColumn($filter_string, $allowed_columns);
        }
    }


    /**
     * @param $filter_string
     * @param $allowed_columns
     * @return bool
     */
    protected function isValidColumn($filter_string, $allowed_columns)
    {
        $temp = explode(':', $filter_string);
        $count_of_elements = count($temp);
        $column_index = $count_of_elements === 3 ? 0 : 1;
        $column = $temp[$column_index];
        if (!in_array($column, $allowed_columns)) {
            $this->getMessageBag()->addWarningMessage('invalid_filter_column', $column);
            return false;
        }
        return true;
    }

}
