<?php

namespace Aptimumio\GroovyServicePattern\Validators;

use Aptimumio\GroovyServicePattern\Traits\MessagesTrait;

/**
 * Validate an order by string to be used in the repository.  This is a mvp version.  When we need to be more stringent
 * we can change it up, but the point is to return messages to the engineer when they compose an invalid string that is not used.
 *
 * Class OrderByValidator
 * @package Aptimumio\GroovyServicePattern\Validators
 */
class OrderByValidator
{

    use MessagesTrait;

    protected $messages = [
        'invalid_type' => 'Order by is not a string.  It should follow the format: name:asc or for multiple name:asc|created_at:desc',
        'invalid_direction' => 'Order by requires a direction of asc or desc as the modifier to the column. eg: name:asc or name:desc',
        'invalid_structure' => 'Order by string is invalid.  It should follow the format: name:asc or for multiple name:asc|created_at:desc'
    ];


    public function __construct()
    {
        $this->initMessages();
    }


    /**
     * @param $order_by
     * @param $allowed_columns
     * @return bool|string
     */
    public function isValid($order_by, $allowed_columns)
    {
        if (!is_string($order_by) || !$order_by) {
            $this->getMessageBag()->addWarningMessage('invalid_type', $this->messages['invalid_type']);
            return false;
        }

        $is_valid_format = $this->isValidFormat($order_by);
        if (!$is_valid_format) {
            return false;
        }

        return $this->excludeInvalidColumns($order_by, $allowed_columns);
    }


    /**
     * Ensure the string looks right.  There are cases where a bad delimiter gets missed here but caught in the next which is equally valid.
     * @param $order_by
     * @return bool
     */
    protected function isValidFormat($order_by)
    {
        $order_by_phrases = explode('|', $order_by);
        $has_structure_error = false;
        $has_direction_error = false;

        foreach ($order_by_phrases as $phrase) {
            if (empty($phrase)) {
                continue;
            }
            $phrase = strtolower($phrase);
            $temp = explode(':', $phrase);
            if (count($temp) === 1) {
                $has_structure_error = true;
                break;
            }
            list($column, $direction) = $temp;
            if (!$column || !$direction) {
                $has_structure_error = true;
                break;
            }
            if (!in_array($direction, ['asc', 'desc'])) {
                $has_direction_error = true;
                break;
            }
        }

        if ($has_structure_error) {
            $this->getMessageBag()->addWarningMessage('invalid_structure', $this->messages['invalid_structure']);
            return false;
        }
        if ($has_direction_error) {
            $this->getMessageBag()->addWarningMessage('invalid_direction', $this->messages['invalid_direction']);
            return false;
        }

        return true;
    }


    /**
     * Return a valid order by string with invalid items removed.  This may result in an empty string.
     * @param $order_by
     * @param $allowed_columns
     * @return string
     */
    protected function excludeInvalidColumns($order_by, $allowed_columns)
    {
        $order_by_phrases = explode('|', $order_by);
        $invalid_order_by_phrases = [];
        $valid_order_by_phrases = array_filter($order_by_phrases, function ($phrase) use ($allowed_columns, &$invalid_order_by_phrases) {
            list($column, $direction) = explode(':', $phrase);
            if (!in_array($column, $allowed_columns)) {
                $invalid_order_by_phrases[] = $phrase;
                return false;
            }
            return true;
        });
        if (count($invalid_order_by_phrases)) {
            foreach ($invalid_order_by_phrases as $phrase) {
                $this->getMessageBag()->addWarningMessage('invalid_order_by_column', $phrase);
            }
        }
        return implode('|', $valid_order_by_phrases);
    }
}


