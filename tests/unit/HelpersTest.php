<?php

class HelpersTest extends \Tests\TestCase
{

    /**
     * @test
     * @group helpers
     */
    public function handle_data_object_test()
    {

        $is_array_function = function ($data) {
            return is_array($data);
        };

        $tests = [
            [
                'data' => ['id' => 1],
                'eval' => $is_array_function,
                'expect' => true,
            ],
            [
                'data' => (object)['id' => 1],
                'eval' => $is_array_function,
                'expect' => true,
            ],
            [
                'data' => new class {
                    public function toArray()
                    {
                        return ['id' => 1];
                    }
                },
                'eval' => $is_array_function,
                'expect' => true,
            ],
            [
                'data' => new class {
                    public function getSum()
                    {
                        return 'Golf Balls';
                    }
                },
                'eval' => function ($data) {
                    return is_object($data);
                },
                'expect' => true,
            ],
            [
                'data' => 'string',
                'eval' => function ($data) {
                    return is_string($data);
                },
                'expect' => true,
            ],
            [
                'data' => null,
                'eval' => function ($data) {
                    return is_null($data);
                },
                'expect' => true,
            ],
        ];

        foreach ($tests as $test) {
            $data = handle_data_object($test['data']);
            $this->assertEquals($test['expect'], $test['eval']($data));
        }
    }
}
