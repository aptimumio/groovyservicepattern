<?php

use Aptimumio\GroovyServicePattern\Validators\FiltersValidator;


class FiltersValidatorTest extends \Tests\TestCase
{

    /**
     * @test
     * @group filtersValidator
     */
    public function hasValidFormats_test()
    {

        $filters = [
            'and:name:=:john',
            [
                'and:type:=:awesome',
                'and:subtype:=:groovy',
                [
                    'type:like:%cool',
                    'or:subtype:like:%just_fine'
                ]
            ],
            'and:created_at:<:2030-01-01'
        ];

        $mock = $this->getMock();
        $result = $mock->hasValidFormats($filters);
        $this->assertEquals(true, $result);
    }

    /**
     * @test
     * @group filtersValidator
     */
    public function getValidColumns_test()
    {
        $filters = [
            'and:name:=:john',
            [
                'and:type:=:awesome',
                'and:subtype:=:groovy',
                [
                    'type:like:%cool',
                    'or:subtype:like:%just_fine'
                ]
            ],
            'and:created_at:<:2030-01-01'
        ];

        $mock = $this->getMock();
        $allowed_columns = ['name', 'typex', 'created_at', 'subtype'];
        $result = $mock->getValidColumns($filters, $allowed_columns);
        $this->assertEquals(true, is_array($result));
    }

    protected function getMock()
    {
        $instance = app()->make(FiltersValidator::class);
        return Mockery::mock($instance)->shouldAllowMockingProtectedMethods();
    }

}
