<?php

use Aptimumio\GroovyServicePattern\Validators\OrderByValidator;

class OrderByValidatorTest extends \Tests\TestCase
{

    /**
     * @test
     * @group orderByValidator
     */
    public function isValidFormat_test()
    {
        // @TODO: Add message tests
        $tests = [
            [
                'argument' => '',
                'expect' => true,
            ],
            [
                'argument' => 'string',
                'expect' => false,
            ],
            [
                'argument' => 'name:down',
                'expect' => false,
            ],
            [
                'argument' => 'name:asc',
                'expect' => true,
            ],
            [
                'argument' => 'name:asc|type:desc',
                'expect' => true,
            ],
            [
                'argument' => 'name:asc!type:desc',
                'expect' => false,
            ]
        ];

        $mock = $this->getMock();
        foreach ($tests as $test) {
            $result = $mock->isValidFormat($test['argument']);
            $this->assertEquals($test['expect'], $result);
        }
    }

    /**
     * @test
     * @group orderByValidator
     */
    public function excludeInvalidColumns_test()
    {
        $tests = [
            [
                'arguments' => [
                    'name:asc',
                    ['name'],
                ],
                'expect' => 'name:asc'
            ],
            [
                'arguments' => [
                    'name:asc|created_at:desc',
                    ['name'],
                ],
                'expect' => 'name:asc'
            ],
            [
                'arguments' => [
                    'name:asc|created_at:desc',
                    ['name', 'created_at'],
                ],
                'expect' => 'name:asc|created_at:desc'
            ],
            [
                'arguments' => [
                    'name:asc|created_at:desc',
                    [],
                ],
                'expect' => ''
            ]
        ];

        $mock = $this->getMock();
        foreach ($tests as $test) {
            $result = $mock->excludeInvalidColumns($test['arguments'][0], $test['arguments'][1]);
            $this->assertEquals($test['expect'], $result);
        }

    }

    protected function getMock()
    {
        $instance = app()->make(OrderByValidator::class); // Runs constructor
        return Mockery::mock($instance)->shouldAllowMockingProtectedMethods();
    }
}
