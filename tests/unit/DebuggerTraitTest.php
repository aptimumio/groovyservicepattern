<?php

use Aptimumio\GroovyServicePattern\Traits\DebuggerTrait;

class DebuggerTraitTest extends \Tests\TestCase
{

    /**
     * @test
     * @group debugger
     */
    public function debugLog_test(){
        $tests = [
            [
                'state' => true,
                'message' => '',
                'data' => ['id' => 1],
                'message_arg' => '',
                'data_arg' => print_r(['id' => 1], true),
                'expect' => true,
            ],
            [
                'state' => true,
                'message' => 'Groovy',
                'data' => ['id' => 1],
                'message_arg' => 'Groovy: ',
                'data_arg' => print_r(['id' => 1], true),
                'expect' => true,
            ],
            [
                'state' => true,
                'message' => 'Groovy',
                'data' => 'groovin',
                'message_arg' => 'Groovy: ',
                'data_arg' => 'groovin',
                'expect' => true,
            ],
            [
                'state' => false,
                'message' => '',
                'data' => ['id' => 1],
                'expect' => false,
            ],
        ];

        // Let's make them objects for simpler typing.
        $tests = array_map(function($test){
            return (object) $test;
        }, $tests);

        $methods = ['debugLog', 'debugEcho'];
        foreach($tests as $test){
            foreach($methods as $method){
                $mock_method_name = $method === 'debugLog' ? 'logOut' : 'echoOut';
                $mock = $this->getMock();
                if($test->state) {
                    $mock->shouldReceive($mock_method_name)
                        ->with($test->message_arg, $test->data_arg)
                        ->once()
                        ->andReturn(true);
                } else {
                    $mock->shouldReceive($mock_method_name)->never();
                }
                $result = $mock
                    ->setDebug($test->state)
                    ->$method($test->data, $test->message);

                $this->assertEquals($test->expect, $result);
            }
        }
    }

    protected function getMock(){

        return Mockery::mock(DebuggerTraitUnitTestClass::class)
            ->shouldAllowMockingProtectedMethods()
            ->makePartial();
    }
}

class DebuggerTraitUnitTestClass{
    use DebuggerTrait;
}
