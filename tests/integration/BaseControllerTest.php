<?php

use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use tests\resources\WidgetsController;
use tests\Traits\WidgetsTestDataTrait;


class BaseControllerTest extends \Tests\TestCase
{

    use DatabaseTransactions;
    use WidgetsTestDataTrait;


    /**
     * @test
     * @group baseController
     */
    public function controller_test_high_limit()
    {
        $widgets = $this->createCollection();
        $request = new Request(['limit' => 200]);
        $controller = app()->make(WidgetsController::class);
        $collection_response = $controller->index($request);
        $temp = json_decode($collection_response->content(), true);
        $collection = data_get($temp, 'data');
        $this->assertSameSize($widgets, $collection);
    }


    /**
     * @test
     * @group baseController
     */
    public function controller_test_default_limit_100()
    {
        $widgets = $this->createCollection();
        $request = new Request();
        $controller = app()->make(WidgetsController::class);
        $collection_response = $controller->index($request);
        $temp = json_decode($collection_response->content(), true);
        $collection = data_get($temp, 'data');
        $this->assertCount(100, $collection);
    }


    /**
     * @test
     * @group baseController
     */
    public function controller_test_paginated()
    {
        $widgets = $this->createCollection();
        $per_page = 10;
        $request = new Request(['is_paginated' => true, 'per_page' => $per_page]);
        $controller = app()->make(WidgetsController::class);
        $collection_response = $controller->index($request);
        $collection_paginated = json_decode($collection_response->content(), true);
        $collection = data_get($collection_paginated, 'data.data');
        $this->assertCount($per_page, $collection);
    }


    /**
     * @test
     * @group baseController
     */
    public function controller_limit_and_offset()
    {
        $widgets = $this->createCollection();
        $request = new Request(['limit' => 3, 'offset' => 63]);
        $controller = app()->make(WidgetsController::class);
        $collection_response = $controller->index($request);
        $temp = json_decode($collection_response->content(), true);
        $collection = data_get($temp, 'data');
        $this->assertCount(3, $collection);
    }


    /**
     * @test
     * @group baseController
     */
    public function controller_test_filtering()
    {
        $widgets = $this->createCollection();
        $count_wholesale_widgets = collect($widgets)->where('source_type', 'wholesale')->count();
        $request = new Request(['limit' => 200, 'filter' => ['and:source_type:=:wholesale']]);
        $controller = app()->make(WidgetsController::class);
        $collection_response = $controller->index($request);
        $temp = json_decode($collection_response->content(), true);
        $collection = data_get($temp, 'data');
        $this->assertCount($count_wholesale_widgets, $collection);
    }


    /**
     * @test
     * @group baseController
     */
    public function controller_test_sorting()
    {
        $widgets = $this->createCollection();
        $alpha = explode(',', 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z');

        $is_array_alphabetical = function ($collection, $direction = 'asc') use ($alpha) {
            if ($direction == 'desc') {
                $alpha = array_reverse($alpha);
            }
            $last_index = 0;
            return array_reduce($collection, function ($acc, $widget) use ($alpha, &$last_index) {
                if (!$acc) {
                    return $acc;
                }
                $letters = array_reverse(str_split(strtolower($widget['name'])));
                $first_letter = array_pop($letters);
                $alpha_index = array_search($first_letter, $alpha);
                if ($alpha_index < $last_index) {
                    return false;
                }
                $last_index = $alpha_index;
                return $acc;
            }, true);
        };

        $directions = ['asc', 'desc'];
        foreach ($directions as $direction) {
            $request = new Request(['limit' => 200, 'order_by' => 'name:' . $direction]);
            $controller = app()->make(WidgetsController::class);
            $collection_response = $controller->index($request);
            $temp = json_decode($collection_response->content(), true);
            $collection = data_get($temp, 'data');
            $this->assertEquals(true, $is_array_alphabetical($collection, $direction));
        }
    }


    /**
     * @test
     * @group baseController
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function controller_test_validation()
    {
        $this->createTables();
        $tests = [
            [
                'data' => [
                    'name' => 'Abcd', // fails on min length validation
                    'code' => 'super',
                    'source_type' => 'wholesale',
                    'source_id' => 1,
                ],
                'expectations' => [
                    'status_code' => 422,
                    'equals' => function($data){
                        return array_key_exists('errors', $data) && array_key_exists('name', $data['errors']);
                    }
                ]
            ],
            [
                'data' => [
                    'name' => 'Abcdefg',
                    'code' => 'super',
                    'source_type' => 'wholesale',
                    'source_id' => 1,
                ],
                'expectations' => [
                    'status_code' => 200,
                    'equals' => function($data){
                        return array_key_exists('data', $data) && array_key_exists('name', $data['data']);
                    }
                ]
            ]
        ];

        foreach($tests as $test){
            $request = new Request($test['data']);
            $controller = app()->make(WidgetsController::class);
            $create_response = $controller->store($request);
            $status_code = $create_response->getStatusCode();
            $data = json_decode($create_response->content(), true);
            $this->assertEquals($test['expectations']['status_code'], $status_code);
            $this->assertEquals(true, $test['expectations']['equals']($data));
        }
    }


    /**
     * @test
     * @group baseController
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function controller_test_update()
    {
        $this->createTables();

        $create_data = [
            'name' => 'Test123',
            'code' => 'super',
            'source_type' => 'wholesale',
            'source_id' => 1,
        ];

        $request = new Request($create_data);
        $controller = app()->make(WidgetsController::class);
        $create_response = $controller->store($request);
        $widget = data_get(json_decode($create_response->getContent(), true), 'data');

        $tests = [
            [
                'data' => [
                    'name' => 'Abcd', // fails on min length validation
                    'code' => 'super',
                    'source_type' => 'wholesale',
                    'source_id' => 1,
                ],
                'id' => data_get($widget, 'id'),
                'expectations' => [
                    'status_code' => 422,
                    'equals' => function($data){
                        return array_key_exists('errors', $data) && array_key_exists('name', $data['errors']);
                    }
                ]
            ],
            [
                'data' => [
                    'name' => 'Abcdefg',
                    'code' => 'super',
                    'source_type' => 'wholesale',
                    'source_id' => 1,
                ],
                'id' => data_get($widget, 'id'),
                'expectations' => [
                    'status_code' => 200,
                    'equals' => function($data){
                        return array_key_exists('data', $data) && array_key_exists('name', $data['data']);
                    }
                ]
            ]
        ];

        foreach($tests as $test){
            $request = new Request($test['data']);
            $controller = app()->make(WidgetsController::class);
            $update_response = $controller->update($request, $test['id']);
            $status_code = $update_response->getStatusCode();
            $data = json_decode($update_response->content(), true);
            $this->assertEquals($test['expectations']['status_code'], $status_code);
            $this->assertEquals(true, $test['expectations']['equals']($data));
        }
    }


    /**
     * @test
     * @group baseController
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function controller_test_transformation()
    {
        $this->createTables();
        $tests = [
            [
                'data' => [
                    'name' => 'Abcdefg',
                    'code' => 'su',
                    'source_type' => 'wholesale',
                    'source_id' => 1,
                ],
                'expectations' => [
                    'status_code' => 200,
                    'equals' => function($data){
                        return array_key_exists('data', $data) && array_key_exists('name', $data['data']);
                    },
                    'outboundTransform' => function($data){
                        return array_key_exists('data', $data) && array_key_exists('cool_new_property', $data['data']);
                    }
                ]
            ]
        ];

        foreach($tests as $test){
            $request = new Request($test['data']);
            $controller = app()->make(WidgetsController::class);
            $transform_response = $controller->store($request);
            $status_code = $transform_response->getStatusCode();
            $data = json_decode($transform_response->content(), true);
            $this->assertEquals($test['expectations']['status_code'], $status_code);
            $this->assertEquals(true, $test['expectations']['equals']($data));
            $this->assertEquals(true, $test['expectations']['outboundTransform']($data));
        }
    }


    /**
     * @test
     * @group baseController
     */
    public function controller_test_eager_loads(){
        $tests = [
            [
                'request' => ['id' => 1, 'eager_loads' => ['widgetSuppliers'], 'debug' => 1],
                'expect' => 3,
                'expectFunction' => function($data){
                    return count(data_get($data, 'data.widget_suppliers', []));
                }
            ],
            [
                'request' => ['id' => 1],
                'expect' => true,
                'expectFunction' => function($data){
                    return !data_get($data, 'data.widget_suppliers', false);
                }
            ],
        ];
        $this->createCollection(true);
        foreach($tests as $test){
            $request = new Request($test['request']);
            $id = data_get($test['request'], 'id');
            $controller = app()->make(WidgetsController::class);
            $response = $controller->show($request, $id);
            $content = json_decode($response->getContent(), true);
            $this->assertEquals(3, $test['expectFunction']($content));
        }
    }
}
