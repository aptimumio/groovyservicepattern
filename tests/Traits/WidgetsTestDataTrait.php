<?php

namespace tests\Traits;

use tests\resources\Widget;
use tests\resources\WidgetSupplier;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

trait WidgetsTestDataTrait
{
    protected function createCollection($with_relationship = false)
    {
        $this->createTables();

        if(!$with_relationship){
            return Widget::factory()->count(125)->create();
        }
        return Widget::factory()->count(125)->has(WidgetSupplier::factory()->count(3))->create();
    }

    protected function createTables()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('code', 100);
            $table->string('source_type', 100);
            $table->bigInteger('source_id');
            $table->timestamps();
        });

        Schema::create('widget_suppliers', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->string('type');
            $table->bigInteger('widget_id');
            $table->timestamps();
        });
    }
}
