<?php

namespace tests\resources;

use Aptimumio\GroovyServicePattern\Bases\BaseModel;
use Aptimumio\GroovyServicePattern\Bases\BaseController;
use Aptimumio\GroovyServicePattern\Bases\BaseService;
use Aptimumio\GroovyServicePattern\Bases\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Widget extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'name',
        'code',
        'source_type',
        'source_id',
    ];

    protected array $validation_data = [
        'rules' => [
            'default' => [
                'name' => 'required|min:6',
                'code' => 'required|min:4',
                'source_type' => 'in:wholesale,retail',
                'source_id' => 'required|int|min:1'
            ]
        ]
    ];

    protected static function newFactory()
    {
        return WidgetFactory::new();
    }

    protected $merge_columns_to_filterable_columns = 'mergeFillableToFilterableColumns';

    protected $merge_columns_to_sortable_columns = 'mergeFillableToSortableColumns';

    public function mergeFillableToFilterableColumns($columns, $filterable_columns)
    {
        return array_merge($this->fillable, $filterable_columns, ['id']);
    }

    public function mergeFillableToSortableColumns($columns, $filterable_columns)
    {
        return array_merge($this->fillable, $filterable_columns, ['id']);
    }

    public function widgetSuppliers(){
        return $this->hasMany(WidgetSupplier::class);
    }
}

class WidgetSupplier extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
    ];

    protected array $validation_data = [
        'rules' => [
            'default' => [
                'name' => 'required|min:6',
                'type' => 'in:wholesale,retail',
            ]
        ]
    ];

    protected static function newFactory()
    {
        return WidgetSupplierFactory::new();
    }

    protected $merge_columns_to_filterable_columns = 'mergeFillableToFilterableColumns';

    protected $merge_columns_to_sortable_columns = 'mergeFillableToSortableColumns';

    public function mergeFillableToFilterableColumns($columns, $filterable_columns)
    {
        return array_merge($this->fillable, $filterable_columns, ['id']);
    }

    public function mergeFillableToSortableColumns($columns, $filterable_columns)
    {
        return array_merge($this->fillable, $filterable_columns, ['id']);
    }
}

class WidgetsRepository extends BaseRepository
{
    public function __construct(Widget $widget)
    {
        $this->model = $widget;
        parent::__construct();

        $this->registerBeforeEventHook($this, 'extendCode', $this->action_hook_event_types['create'], 1);
        $this->registerAfterEventHook($this, 'addProperty', $this->action_hook_event_types['create'], 1);
    }

    public function extendCode($input_data, $entity, $action){
        $code = data_get($input_data, 'code', '');
        $code = str_pad($code, 4, '_', STR_PAD_RIGHT);
        $input_data['code'] = $code;
        return $input_data;
    }

    public function addProperty($input_data, $entity, $action){
        $entity->cool_new_property = true;
        return $entity;
    }

}

class WidgetsService extends BaseService
{
    public function __construct(WidgetsRepository $widgets_repository)
    {
        $this->repository = $widgets_repository;
        parent::__construct();
    }
}

class WidgetsController extends BaseController
{
    public function __construct(WidgetsService $widgets_service)
    {
        $this->service = $widgets_service;
        parent::__construct();
    }
}

class WidgetFactory extends Factory
{
    protected $model = Widget::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $now = \Carbon\Carbon::now();
        return [
            'name' => $this->faker->name,
            'code' => $this->faker->hexColor,
            'source_type' => $this->faker->randomElement(['wholesale', 'manufactured']),
            'source_id' => $this->faker->numberBetween(0, 1000000),
            'created_at' => $now,
            'updated_at' => $now,
        ];
    }
}

class WidgetSupplierFactory extends Factory
{
    protected $model = WidgetSupplier::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $now = \Carbon\Carbon::now();
        return [
            'name' => $this->faker->name,
            'type' => $this->faker->randomElement(['wholesale', 'manufactured']),
            'created_at' => $now,
            'updated_at' => $now,
        ];
    }
}
